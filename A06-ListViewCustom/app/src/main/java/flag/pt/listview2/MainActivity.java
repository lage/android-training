package flag.pt.listview2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listview);

        final ArrayList<Pessoa> pessoas = new ArrayList<>();
        pessoas.add(new Pessoa("Bruno", "Varzim", 28));
        pessoas.add(new Pessoa("Hugo", "Lisboa", 284));
        pessoas.add(new Pessoa("Joao", "Porto", 228));
        pessoas.add(new Pessoa("Juliana", "Aveiro", 8));
        pessoas.add(new Pessoa("Tiago", "Algarve", 248));
        pessoas.add(new Pessoa("Carlos", "Varzim", 278));

        for (int i = 0; i < 10000; i++) {
            pessoas.add(new Pessoa("Pessoa "+i, "Cidade "+i, i));
        }


        PessoasAdapter pessoasAdapter = new PessoasAdapter(this, pessoas);

        listView.setAdapter(pessoasAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Pessoa pessoa = pessoas.get(position);
                Log.d("ListView", "onItemClick: " + position + " - "+ pessoa);

//                Intent intent = new Intent(MainActivity.this, DetalhesActivity.class);
//                intent.putExtra("pessoa", pessoa);
//                startActivity(intent);

            }
        });
    }
}

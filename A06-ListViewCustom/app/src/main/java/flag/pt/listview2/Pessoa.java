package flag.pt.listview2;

/**
 * Created by DEVELOPER on 02/02/2017.
 */

public class Pessoa {
    private String nome;
    private String localidade;
    private int idade;

    public Pessoa(String nome, String localidade, int idade) {
        this.nome = nome;
        this.localidade = localidade;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public String getLocalidade() {
        return localidade;
    }

    public int getIdade() {
        return idade;
    }

    @Override
    public String toString(){
        return nome+"\n"+localidade+" "+idade;
    }
}

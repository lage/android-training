package flag.pt.listview2;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DEVELOPER on 02/02/2017.
 */

public class PessoasAdapter extends BaseAdapter{

    private ArrayList<Pessoa> pessoas;
    private Activity activity;

    public PessoasAdapter(Activity activity, ArrayList<Pessoa> pessoas) {
        this.pessoas = pessoas;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return pessoas.size();
    }

    @Override
    public Object getItem(int position) {
        return pessoas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("PessoasAdapter", "getView: "+position);
        View view = null;
        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_pessoa, parent, false);
            //Toast.makeText(context, "NOVA", Toast.LENGTH_SHORT).show();
        } else {
            view = convertView;
            //Toast.makeText(context, "RECICLADA", Toast.LENGTH_SHORT).show();
        }

        Pessoa pessoa = pessoas.get(position);


        TextView nomeView = (TextView) view.findViewById(R.id.nome);
        nomeView.setText(pessoa.getNome());

        TextView localidade = (TextView) view.findViewById(R.id.localidade);
        localidade.setText(pessoa.getLocalidade());

        TextView idadeView = (TextView) view.findViewById(R.id.idade);
        idadeView.setText(pessoa.getIdade()+"");

        return view;
    }
}

package flag.pt.internetasynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Created by DEVELOPER on 04/02/2017.
 */

public class InternetAsyncTask extends AsyncTask<Void, Integer, String>{

    MainActivity mainActivity;

    public InternetAsyncTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mainActivity.atualizaInicio("Vou fazer um pedido");
    }

    @Override
    protected String doInBackground(Void... params) {

        String result = null;

        try {
            URL url = new URL("http://api.icndb.com/jokes/random/100");

            URLConnection connection = url.openConnection();

            Scanner scanner = new Scanner(connection.getInputStream());


            StringBuilder content = new StringBuilder();
            while (scanner.hasNextLine()) {
                content.append(scanner.nextLine());
            }

            result = content.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        mainActivity.atualizaFim(result);
    }
}

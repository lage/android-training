package flag.pt.internetasynctask;

/**
 * Created by DEVELOPER on 04/02/2017.
 */

public class Joke {

    private int id;
    private String conteudo;

    public Joke(int id, String conteudo) {
        this.id = id;
        this.conteudo = conteudo;
    }

    public int getId() {
        return id;
    }

    public String getConteudo() {
        return conteudo;
    }

    @Override
    public String toString() {
        return id+": "+conteudo;
    }
}

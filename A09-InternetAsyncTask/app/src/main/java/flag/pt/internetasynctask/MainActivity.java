package flag.pt.internetasynctask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    ListView listViewPiadas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.text_view);

        listViewPiadas = (ListView) findViewById(R.id.listview);
    }

    public void atualizaInicio(String text){
        textView.setText(text);
    }

    public void atualizaFim(String result){

        ArrayList<Joke> piadas = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonValues = jsonObject.getJSONArray("value");

            for(int i=0; i < jsonValues.length(); i++){
                JSONObject object = jsonValues.getJSONObject(i);

                int id = object.getInt("id");
                String conteudo = object.getString("joke");

                Joke piada = new Joke(id, conteudo);
                piadas.add(piada);
                Log.d("MainActivity", "atualiza: "+piada);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            textView.setText("Error happened!");
        }


        //listview and adapter
        //- criar listview no layout
        //- criar adapter
        //- associar adapter à listview

        ArrayAdapter<Joke> adapterPiadas =
                new ArrayAdapter<Joke>(this, android.R.layout.simple_list_item_1, piadas);
        listViewPiadas.setAdapter(adapterPiadas);
    }

    public void onClick(View view) {

        InternetAsyncTask task = new InternetAsyncTask(this);
        task.execute();

    }
}

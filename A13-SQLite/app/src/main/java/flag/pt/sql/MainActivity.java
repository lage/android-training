package flag.pt.sql;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import flag.pt.sql.persistencia.FilmeDataSource;
import flag.pt.sql.persistencia.MyDatabaseHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MyDatabaseHelper databaseHelper = new MyDatabaseHelper(this);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        FilmeDataSource filmeDataSource = new FilmeDataSource(database);

        filmeDataSource.insereFilme(new Filme("Old Boy", 1879, "Descricao do OldBoy"));

        ArrayList<Filme> filmeArrayList = filmeDataSource.leTodosFilmes();

        for (Filme filme : filmeArrayList) {
            Log.d("MainActivity", "onCreate: "+filme);
        }
    }
}

package flag.pt.sql;

/**
 * Created by DEVELOPER on 09/02/2017.
 */

public class Filme {
    private int id;
    private String titulo;
    private int anoLancamento;
    private String descricao;

    public Filme(int id, String titulo, int anoLancamento, String descricao) {
        this.id = id;
        this.titulo = titulo;
        this.anoLancamento = anoLancamento;
        this.descricao = descricao;
    }

    public Filme(String titulo, int anoLancamento, String descricao) {
        this.titulo = titulo;
        this.anoLancamento = anoLancamento;
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getAnoLancamento() {
        return anoLancamento;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return "Filme{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", anoLancamento=" + anoLancamento +
                ", descricao='" + descricao + '\'' +
                '}';
    }
}

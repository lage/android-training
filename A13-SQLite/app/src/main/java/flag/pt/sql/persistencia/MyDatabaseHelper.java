package flag.pt.sql.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by DEVELOPER on 09/02/2017.
 */
public class MyDatabaseHelper  extends SQLiteOpenHelper{


    public MyDatabaseHelper(Context context) {
        super(context, "db_filmes", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE filmes (" +
                "_id INTEGER PRIMARY KEY, " +
                "titulo TEXT, " +
                "ano_lancamento INTEGER, " +
                "descricao TEXT)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

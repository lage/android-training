package flag.pt.sql.persistencia;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import flag.pt.sql.Filme;

/**
 * Created by DEVELOPER on 09/02/2017.
 */

public class FilmeDataSource {

    private SQLiteDatabase database;

    public FilmeDataSource(SQLiteDatabase database) {
        this.database = database;
    }

    public ArrayList<Filme> leTodosFilmes(){
        ArrayList<Filme> filmeArrayList = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM filmes", null);
        if(cursor.getCount() == 0){
            return filmeArrayList;
        }

        cursor.moveToFirst();

        do{
            int id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
            String titulo = cursor.getString(cursor.getColumnIndexOrThrow("titulo"));
            String descricao = cursor.getString(cursor.getColumnIndexOrThrow("descricao"));
            int anoLancamento = cursor.getInt(cursor.getColumnIndexOrThrow("ano_lancamento"));

            Filme filme = new Filme(id, titulo, anoLancamento, descricao);
            filmeArrayList.add(filme);
        }while (cursor.moveToNext());

        return filmeArrayList;
    }

    public Filme leFilme(int id){
        Cursor cursor = database.rawQuery("SELECT * FROM filmes WHERE _id = ?",
                new String[]{String.valueOf(id)});

        if(cursor.getCount() == 0){
            return null;
        }

        cursor.moveToFirst();

        String titulo = cursor.getString(cursor.getColumnIndexOrThrow("titulo"));
        String descricao = cursor.getString(cursor.getColumnIndexOrThrow("descricao"));
        int anoLancamento = cursor.getInt(cursor.getColumnIndexOrThrow("ano_lancamento"));

        Filme filme = new Filme(id, titulo, anoLancamento, descricao);

        return filme;
    }

    public void insereFilme(Filme filme){

        ContentValues values = new ContentValues();
        values.put("titulo", filme.getTitulo());
        values.put("ano_lancamento", filme.getAnoLancamento());
        values.put("descricao", filme.getDescricao());

        database.insert("filmes", null, values);
    }

    public void atualizaFilme(Filme filme){
        ContentValues values = new ContentValues();
        values.put("titulo", filme.getTitulo());
        values.put("ano_lancamento", filme.getAnoLancamento());
        values.put("descricao", filme.getDescricao());

        database.update("filmes", values, "_id = ?", new String[] { String.valueOf(filme.getId()) });
    }

    public void apagarFilme(int id){
        database.delete("filmes", "_id="+id, null );
    }
}

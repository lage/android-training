package flag.pt.notas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NotaDetailsActivity extends AppCompatActivity {

    int notaPosicao;
    Nota notaSelecionada;

    EditText tituloView;
    EditText descricaoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota_details);


        notaPosicao = getIntent().getIntExtra("pos", -1);

        notaSelecionada = FakeDataBase.notas.get(notaPosicao);

        tituloView = (EditText) findViewById(R.id.edit_title);
        descricaoView = (EditText) findViewById(R.id.edit_description);

        tituloView.setText(notaSelecionada.getTitulo());
        descricaoView.setText(notaSelecionada.getDescição());
    }

    public void onGuardar(View view) {
        String strTitulo = tituloView.getText().toString();
        notaSelecionada.setTitulo(strTitulo);

        String strDescr = descricaoView.getText().toString();
        notaSelecionada.setDescição(strDescr);

        finish();
    }

    public void onApagar(View view) {
        FakeDataBase.notas.remove(notaPosicao);

        finish();
    }
}

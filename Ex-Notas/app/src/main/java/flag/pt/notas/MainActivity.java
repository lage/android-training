package flag.pt.notas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

     NotasAdapter notasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FakeDataBase.notas.add(new Nota("Nota 1", "Descição 1"));
        FakeDataBase.notas.add(new Nota("Ir as compras", "leite, cereais, etc"));
        FakeDataBase.notas.add(new Nota("Ir a flag", "java, android, projeto, java, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projetojava, android, projeto"));

        ListView listView = (ListView) findViewById(R.id.list_view);

        notasAdapter =
                new NotasAdapter(this, FakeDataBase.notas);

        listView.setAdapter(notasAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //abrir outra activity
                Intent intent = new Intent(MainActivity.this, NotaDetailsActivity.class);
                intent.putExtra("pos", position);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        notasAdapter.notifyDataSetChanged();
    }

    public void onAdicionar(View view) {
        Intent intent = new Intent(this, NovaNotaActivity.class);
        startActivity(intent);
    }
}

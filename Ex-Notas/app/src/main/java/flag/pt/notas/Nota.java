package flag.pt.notas;

/**
 * Created by DEVELOPER on 04/02/2017.
 */

public class Nota {

    private String titulo;
    private String descição;

    public Nota(String titulo, String descição) {
        this.titulo = titulo;
        this.descição = descição;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescição() {
        return descição;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescição(String descição) {
        this.descição = descição;
    }

    @Override
    public String toString() {
        return "Nota{" +
                "titulo='" + titulo + '\'' +
                ", descição='" + descição + '\'' +
                '}';
    }
}

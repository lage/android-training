package flag.pt.notas;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DEVELOPER on 04/02/2017.
 */

public class NotasAdapter extends BaseAdapter{

    private Context activity;
    private ArrayList<Nota> listaNotas;

    public NotasAdapter(Context activity, ArrayList<Nota> listaNotas) {
        this.activity = activity;
        this.listaNotas = listaNotas;
    }

    @Override
    public int getCount() {
        return listaNotas.size();
    }

    @Override
    public Object getItem(int position) {
        return listaNotas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_nota, parent, false);
        } else {
            view = convertView;
        }

        TextView tituloView = (TextView) view.findViewById(R.id.item_title);
        TextView descricaoView = (TextView) view.findViewById(R.id.item_descricao);

        Nota nota = listaNotas.get(position);

        tituloView.setText(nota.getTitulo());
        descricaoView.setText(nota.getDescição());

        return view;
    }
}

package flag.pt.notas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NovaNotaActivity extends AppCompatActivity {

    EditText titleView;
    EditText descView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_nota);

        titleView = (EditText) findViewById(R.id.edit_nova_title);
        descView = (EditText) findViewById(R.id.edit_nova_descricao);
    }

    public void onNova(View view) {
        String titulo = titleView.getText().toString();
        String descicao = descView.getText().toString();
        Nota novaNota = new Nota(titulo, descicao);
        FakeDataBase.notas.add(novaNota);

        finish();
    }
}

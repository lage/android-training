package flag.pt.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetalhesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        TextView textView = (TextView) findViewById(R.id.pessoa);

        String pessoa = getIntent().getStringExtra("pessoa");

     
        textView.setText(pessoa);
    }
}

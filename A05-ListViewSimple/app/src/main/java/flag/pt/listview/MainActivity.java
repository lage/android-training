package flag.pt.listview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listview);

        final ArrayList<String> nomes = new ArrayList<>();
        nomes.add("Bruno");
        nomes.add("Carlos");
        nomes.add("Hugo");
        nomes.add("João");
        nomes.add("Paulo");
        nomes.add("Juliana");
        nomes.add("Stefen");
        nomes.add("Tiago");
        nomes.add("a");
        nomes.add("b");
        nomes.add("c");
        nomes.add("d");
        nomes.add("f");
        nomes.add("g");


        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nomes);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String nome = nomes.get(position);
                Log.d("ListView", "onItemClick: " + position + " - "+ nome);

                Intent intent = new Intent(MainActivity.this, DetalhesActivity.class);
                intent.putExtra("pessoa", nome);
                startActivity(intent);

            }
        });

    }
}

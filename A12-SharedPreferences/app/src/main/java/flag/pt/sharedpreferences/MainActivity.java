package flag.pt.sharedpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editTextView;
    EditText editTextNumeroView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextView = (EditText) findViewById(R.id.edit_text);
        editTextNumeroView = (EditText) findViewById(R.id.edit_text_numero);

    }

    public void onGuardar(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("edit_text_string", editTextView.getText().toString());

        String inteiroStr = editTextNumeroView.getText().toString();
        int inteiro = Integer.parseInt(inteiroStr);

        editor.putInt("edit_text_int", inteiro);

        editor.apply();
    }

    public void onCarregar(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences("pref", MODE_PRIVATE);

        String valor = sharedPreferences.getString("edit_text_string", "default");
        editTextView.setText(valor);

        int valorInteiro = sharedPreferences.getInt("edit_text_int", -1);
        editTextNumeroView.setText(valorInteiro+"");
    }
}

package flag.pt.genericinternettask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView pede10TextView;
    TextView pede100TextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pede10TextView = (TextView) findViewById(R.id.pede10_textview);
        pede100TextView = (TextView) findViewById(R.id.pede100_textview);
    }

    public void onPede10(View view) {
        InternetAsyncTask task10 = new InternetAsyncTask("http://api.icndb.com/jokes/random/10");
        task10.setListener(new InternetAsyncTask.Listener() {
            @Override
            public void onSuccess(String result) {
                pede10TextView.setText("Sucesso!");
            }

            @Override
            public void onError() {
                pede10TextView.setText("Erro!");
            }
        });

        task10.execute();
    }

    public void onPede100(View view) {
        InternetAsyncTask task100 = new InternetAsyncTask("http://api.icndb.com/jokes/random/100");

        task100.setListener(new InternetAsyncTask.Listener() {
            @Override
            public void onSuccess(String result) {
                pede100TextView.setText("Sucesso!");
            }

            @Override
            public void onError() {
                pede100TextView.setText("Erro!");
            }
        });
        task100.execute();
    }
}

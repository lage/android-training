package flag.pt.genericinternettask;

import android.os.AsyncTask;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Created by DEVELOPER on 04/02/2017.
 */
public class InternetAsyncTask extends AsyncTask<Void, Integer, String>{

    interface Listener{
        void onSuccess(String result);

        void onError();
    }

    private String urlEndpoint;
    private Listener listener;

    public InternetAsyncTask(String urlParam) {
        this.urlEndpoint = urlParam;
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... params) {

        String result = null;

        try {
            URL url = new URL(urlEndpoint);

            URLConnection connection = url.openConnection();

            Scanner scanner = new Scanner(connection.getInputStream());


            StringBuilder content = new StringBuilder();
            while (scanner.hasNextLine()) {
                content.append(scanner.nextLine());
            }

            result = content.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(result == null){
            //ocorreu erro
            listener.onError();
        }else{
            //sucesso
            listener.onSuccess(result);
        }
    }
}

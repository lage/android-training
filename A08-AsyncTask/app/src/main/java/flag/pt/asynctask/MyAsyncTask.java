package flag.pt.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by DEVELOPER on 04/02/2017.
 */
public class MyAsyncTask extends AsyncTask<Void, Integer, String>{

    MainActivity mainActivity;

    public MyAsyncTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mainActivity.atualiza("A dormir");
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            Log.d("AsyncTask", "doInBackground: comecar a dormir");
            Thread.sleep(5000);
            Log.d("AsyncTask", "doInBackground: acabei de dormir");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Acordei";
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        mainActivity.atualiza(result);
    }
}

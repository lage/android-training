package flag.pt.asynctask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.atualiza);

    }

    public void atualiza(String resultado){
        textView.setText(resultado);
    }

    public void onClick(View view) throws InterruptedException {

        MyAsyncTask myTask = new MyAsyncTask(this);
        myTask.execute();


    }
}
